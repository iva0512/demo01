-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.21-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 students 的数据库结构
CREATE DATABASE IF NOT EXISTS `students` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `students`;

-- 导出  表 students.students_info 结构
CREATE TABLE IF NOT EXISTS `students_info` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `sex` varchar(6) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  students.students_info 的数据：~0 rows (大约)
DELETE FROM `students_info`;
/*!40000 ALTER TABLE `students_info` DISABLE KEYS */;
INSERT INTO `students_info` (`id`, `name`, `sex`, `age`, `weight`, `height`) VALUES
	(2017011, '周浩鹏', '男', 20, 65, 167),
	(2017012, '陈永兴', '男', 20, 75, 176),
	(2017013, '罗健', '男', 20, 55, 170),
	(2017014, '杨健东', '男', 20, 59, 156),
	(2017015, '何景超', '男', 20, 66, 177),
	(2017016, '杨林', '男', 20, 74, 170),
	(2017017, '邹康', '男', 20, 77, 160),
	(2017018, '唐瀚韬', '男', 20, 71, 168),
	(2017019, '刘弘威', '男',20, 70, 172),
	(10000, '周老师', '男', 40, 80, 170);
/*!40000 ALTER TABLE `students_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
