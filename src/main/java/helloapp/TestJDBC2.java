package helloapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class TestJDBC2 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// 1. 加载驱动
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/ch04_students?useUnicode=true&characterEncoding=utf-8";
		String username = "root";
		String password = "123456";
		// 2. 建立连接
		Connection con = DriverManager.getConnection(url, username, password);
		String sql = "select * from user where username =? and password =? ";
		// 3. 创建Statement
		//Statement stm = con.createStatement();
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, "admin");
		stm.setString(2, "123456");
		// 4. 执行SQL语句
		ResultSet res = stm.executeQuery();
		// 5. 处理结果
		String name= null;
		String pwd = null;
		while(res.next()){
			int id = res.getInt("id");
			 name = res.getString("username");
			  pwd = res.getString("password");
			break;
//			System.out.println("id = " +id );
			
		}
		System.out.println("name = " +name );
		System.out.println("pwd = " +pwd );
		// 6. 释放资源
		res.close();
		stm.close();
		con.close();

	}

}
