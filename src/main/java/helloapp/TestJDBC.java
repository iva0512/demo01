package helloapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class TestJDBC {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// 1. 加载驱动
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/ch04_students?useUnicode=true&characterEncoding=utf-8";
		String username = "root";
		String password = "123456";
		// 2. 建立连接
		Connection con = DriverManager.getConnection(url, username, password);
		String sql = "select * from stu";
		// 3. 创建Statement
		//Statement stm = con.createStatement();
		PreparedStatement stm = con.prepareStatement(sql);
		// 4. 执行SQL语句
		ResultSet res = stm.executeQuery();
		// 5. 处理结果
		while(res.next()){
			int xh = res.getInt("xh");
			String name = res.getString(2);
			int cj = res.getInt("cj");
			System.out.println("xh = " +xh );
			System.out.println("name = " +name );
			System.out.println("cj = " +cj );
		}
		// 6. 释放资源
		res.close();
		stm.close();
		con.close();

	}

}
