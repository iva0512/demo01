package helloapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.helloapp.entity.StudentInfo;

public class TestJDBC4 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// 1. 加载驱动
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/students?useUnicode=true&characterEncoding=utf-8";
		String username = "root";
		String password = "123456";
		// 2. 建立连接
		Connection con = DriverManager.getConnection(url, username, password);
		String sql = "select  `id`, `name`, `sex`, `age`, `weight`, `height` from `students_info`";
		// 3. 创建Statement
		// Statement stm = con.createStatement();
		PreparedStatement stm = con.prepareStatement(sql);
		// 4. 执行SQL语句
		ResultSet res = stm.executeQuery();
		// 5. 处理结果
		List<StudentInfo> lists = new ArrayList<>();
		while (res.next()) {
			int id = res.getInt("id");
			String name = res.getString("name");
			String sex = res.getString("sex");
			int age = res.getInt("age");
			float weight = res.getFloat("weight");
			float height = res.getFloat("height");
			lists.add(new StudentInfo(id, name, sex, age, weight, height));
		}
		for (StudentInfo in : lists) {
			System.out.println(in);
		}
		// 6. 释放资源
		res.close();
		stm.close();
		con.close();
	}
}
