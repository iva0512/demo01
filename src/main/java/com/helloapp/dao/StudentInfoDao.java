package com.helloapp.dao;

import java.io.Serializable;
import java.util.List;

import com.helloapp.entity.StudentInfo;

public interface StudentInfoDao {

	// 增加 
	void add(StudentInfo studentInfo);
	// 删除
	void delete(StudentInfo studentInfo);
	// 修改
	void update(StudentInfo studentInfo);
	// 查询
	List<StudentInfo> queryAll();
	
	/**
	 * 分页查询
	 * @param pos 下标 从0开始
	 * @param page 行数
	 * @return
	 */
		List<StudentInfo> queryPage(int pos,int page);
	// 获取单一对象
	StudentInfo getById(Serializable id);
	int count();
}
