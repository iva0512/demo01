package com.helloapp.dao;

import com.helloapp.entity.User;

public interface UserDao {

	User getByUsernameAndPassword(String username,String password);

	// 不推荐
	void insert(String username, String password);

	void add(User user);
}
