package com.helloapp.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.helloapp.dao.StudentInfoDao;
import com.helloapp.dao.impl.StudentInfoDaoImpl;
import com.helloapp.entity.StudentInfo;

@WebServlet("/show")
public class ListServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// List<StudentInfo> lists ;
		// lists.add(new StudentInfo(1,"1","1",1,1.0f,1.0f));
		// show?page=2&row=5
		StudentInfoDao studentInfoDao = new StudentInfoDaoImpl();
		String page = req.getParameter("page");
		String row = req.getParameter("row");
		int npage = 0;
		int nrow = 0;
		int totalPage = 0;
		if (page == null) {
			npage = 1;
		} else {
			npage = Integer.parseInt(page);
			if (npage < 1)
				npage = 1;
		}

		if (row == null) {
			nrow = 5;
		} else {
			nrow = Integer.parseInt(row);
			int total = studentInfoDao.count();
			totalPage = (total + nrow - 1) / nrow;
			if (npage > totalPage)
				npage = totalPage;
		}

		
		// List<StudentInfo> lists = studentInfoDao.queryAll();
		// 计算下标
		int pos = (npage - 1) * nrow;
		List<StudentInfo> lists = studentInfoDao.queryPage(pos, nrow);
		// 当前页
		req.setAttribute("page", npage);
		req.setAttribute("list", lists);
		req.setAttribute("totalPage", totalPage);
		req.getRequestDispatcher("list.jsp").forward(req, resp);
	}
}
