package com.helloapp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.helloapp.dao.StudentInfoDao;
import com.helloapp.dao.impl.StudentInfoDaoImpl;
import com.helloapp.entity.StudentInfo;

@WebServlet("/addStudent")
public class AddStudentSevlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("addStudent.jsp").forward(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 编码
		//req.setCharacterEncoding("utf-8");
		// 获取值
		String id = req.getParameter("id");
		String name = req.getParameter("name");
		String sex = req.getParameter("sex");
		String age = req.getParameter("age");
		String height = req.getParameter("height");
		String weight = req.getParameter("weight");
		// 封装对象
		StudentInfo studentInfo = new StudentInfo(Integer.parseInt(id),name,sex,Integer.parseInt(age),Float.parseFloat(height),Float.parseFloat(weight));
		// 调用Dao接口
		StudentInfoDao studentInfoDao = new StudentInfoDaoImpl();
		studentInfoDao.add(studentInfo);
		// 中转
		resp.sendRedirect("show");
	}
}
