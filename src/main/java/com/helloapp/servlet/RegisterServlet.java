package com.helloapp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.helloapp.dao.UserDao;
import com.helloapp.dao.impl.UserDaoImpl;
import com.helloapp.entity.User;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/zhou")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//request.setCharacterEncoding("utf-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		//CTRL+ SHIFT + O  导包 
		UserDao userDao = new UserDaoImpl();
		userDao.add(new User(null,username,password));
		
	}

}
