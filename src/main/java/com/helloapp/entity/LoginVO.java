package com.helloapp.entity;
//"{ \"url\":\"main.jsp\",\"type\":1}";
public class LoginVO {

	private String url;
	private Integer type;
	
	
	public LoginVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginVO(String url, Integer type) {
		super();
		this.url = url;
		this.type = type;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
}
