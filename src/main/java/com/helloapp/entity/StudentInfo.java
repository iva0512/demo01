package com.helloapp.entity;
/**
 * 驼峰命名法
 * 类名都首字母大写，后单词首字母大写
 * 方法名首字母小写，后单词首字母大写
 * 属性名首字母小写，后单词首字母大写
 * @author ZTE
 *
 */
public class StudentInfo {
	private Integer id;
	private String name;
	private String sex;
	private Integer age;
	private Float weight;
	private Float height;
	//private List<StudentInfo> list;
	public StudentInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "StudentInfo [id=" + id + ", name=" + name + ", sex=" + sex + ", age=" + age + ", weight=" + weight
				+ ", height=" + height + "]";
	}
	public StudentInfo(Integer id, String name, String sex, Integer age, Float weight, Float height) {
		super();
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.weight = weight;
		this.height = height;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public Float getHeight() {
		return height;
	}
	public void setHeight(Float height) {
		this.height = height;
	}
}
