<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>

<title>用户注册</title>
</head>
<body>
<%@ include file='header.jsp' %>
	<div class="container" style="margin-top: 60px">
		<div class="row" style="margin-top: 100px">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<span class="glyphicon glyphicon-console"></span> Register
						</h3>
					</div>
					<div class="panel-body">
						
						<form id="login" action="doregister.jsp" method="post">
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control" id="username"
										name="username"
										placeholder="用户名">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-eye-close"></span>
									</div>
									<input type="password" class="form-control" id="password"
										name="password" placeholder="密码">
								</div>
							</div>
								<div class="form-group">
									<div class="col-md-6 col-md-offset-3">
										<button type="submit" class="btn btn-primary btn-block">注册</button>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>