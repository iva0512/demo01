<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- <%@ page import="java.sql.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="com.helloapp.entity.StudentInfo"%> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>

	<%-- <%
		//1. 加载驱动
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/ch04_students?useUnicode=true&characterEncoding=utf-8";
		String username = "root";
		String password = "123456";
		// 2. 建立连接
		Connection con = DriverManager.getConnection(url, username, password);
		String sql = "select  `id`, `name`, `sex`, `age`, `weight`, `height` from `students_info`";
		// 3. 创建Statement
		PreparedStatement stm = con.prepareStatement(sql);
		// 4. 执行SQL语句
		ResultSet res = stm.executeQuery();
		// 5. 处理结果
		List<StudentInfo> lists = new ArrayList<>();
		while (res.next()) {
			int id = res.getInt("id");
			String name = res.getString("name");
			String sex = res.getString("sex");
			int age = res.getInt("age");
			float weight = res.getFloat("weight");
			float height = res.getFloat("height");
			lists.add(new StudentInfo(id, name, sex, age, weight, height));
		}
		/* for (StudentInfo in : lists) {
			System.out.println(in);
		} */
		// 6. 释放资源
		res.close();
		stm.close();
		con.close();
	%> --%>
	<table class="table table-striped">
		<tr>
			<th>学号</th>
			<th>姓名</th>
			<th>性别</th>
			<th>年龄</th>
			<th>体重</th>
			<th>身高</th>
		</tr>
		<!-- <tr>
<td>1</td>
<td>周</td>
<td>男</td>
<td>40</td>
<td>70</td>
<td>170</td>
</tr> -->

	<c:forEach items="${list}" var="in">
	<tr>
		<td>${in.id }</td>
		<td>${in.name }</td>
		<td>${in.sex }</td>
		<td>${in.age }岁</td>
		<td>${in.weight }kg</td>
		<td>${in.height}cm</td>
	</tr>
	</c:forEach>
<%-- <% List<StudentInfo> lists = (List<StudentInfo>)request.getAttribute("list"); %>
		<%
			for (StudentInfo in : lists) {
				out.print("<tr>");
				out.print("<td>"+ in.getId()+"</td>");
				out.print("<td>"+ in.getName()+"</td>");
				out.print("<td>"+in.getSex()+"</td>");
				out.print("<td>"+in.getAge()+"</td>");
				out.print("<td>"+in.getWeight()+"</td>");
				out.print("<td>"+in.getHeight()+"</td>");
				out.print("</tr>");
				//System.out.println(in);
			}
		%>
 --%>
	</table>
    <ul class="pagination pagination-lg" style="float:right;">
         <li><a href="show?page=${page-1 }&row=5">&laquo;</a></li>
         <li><a href="#">共${totalPage }页</a></li>
         <li><a href="#">当前${page }页</a></li>
         <li><a href="show?page=${page+1 }&row=5">&raquo;</a></li>
    </ul>
</body>
</html>