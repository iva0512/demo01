<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 定义JavaBean -->
	<jsp:useBean id="zhou" class="com.helloapp.entity.Add" scope="request"></jsp:useBean>
	<!-- 设置JavaBean属性值 *代表所有属性 -->
	<jsp:setProperty property="*" name="zhou"/>
	<!-- 获取JavaBean属性值  -->
	<jsp:getProperty property="shuju1" name="zhou"/>+<jsp:getProperty property="shuju2" name="zhou"/>=<jsp:getProperty property="sum" name="zhou"/>
	<%-- <%
	//设置中文乱码问题，utf-8与网页编码对应 
	request.setCharacterEncoding("utf-8");
	String shuju1 = request.getParameter("shuju1");
	String shuju2 = request.getParameter("shuju2");

	int sum = Integer.parseInt(shuju1) + Integer.parseInt(shuju2);

	out.print(shuju1 + " + " + shuju2 + " = " + sum);
	%> --%>

</body>
</html>