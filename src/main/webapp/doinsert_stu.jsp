<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
	//设置中文乱码问题，utf-8与网页编码对应 
	request.setCharacterEncoding("utf-8");
	String id = request.getParameter("id");
	String name = request.getParameter("name");
	String sex = request.getParameter("sex");
	String age = request.getParameter("age");
	String weight = request.getParameter("weight");
	String height = request.getParameter("height");
	System.out.println(id);
	System.out.println(name);
	System.out.println(sex);
	System.out.println(age);
	System.out.println(weight);
	System.out.println(height);

	// 1. 加载驱动
	Class.forName("com.mysql.cj.jdbc.Driver");
	String url = "jdbc:mysql://localhost:3306/students?useUnicode=true&characterEncoding=utf-8";
	String username = "root";
	String password = "123456";
	// 2. 建立连接
	Connection con = DriverManager.getConnection(url, username, password);
	String sql = "INSERT INTO `students_info` (`id`, `name`, `sex`, `age`, `weight`, `height`) VALUES (?, ?, ?, ?, ?, ?);";
	// 3. 创建Statement
	PreparedStatement stm = con.prepareStatement(sql);
	stm.setInt(1, Integer.parseInt(id));
	stm.setString(2, name);
	stm.setString(3,sex);
	stm.setInt(4, Integer.parseInt(age));
	stm.setFloat(5, Float.parseFloat(weight));
	stm.setFloat(6, Float.parseFloat(height));
	// 4. 执行SQL语句
	int result = stm.executeUpdate();
	// 5. 处理结果
	if (result == 1) {
		System.out.println("插入成功");
	} else {
		System.out.println("插入失败");
	}

	// 6. 释放资源
	stm.close();
	con.close();
%>