<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%
	//设置中文乱码问题，utf-8与网页编码对应 
	request.setCharacterEncoding("utf-8");
	String username = request.getParameter("username");
	String password = request.getParameter("password");
	// 根据传递信息获取对象
	if (username != null && password != null) {
		// 成功获取，保存到数据库
		// 1. 加载驱动
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/ch04_students?useUnicode=true&characterEncoding=utf-8";
		// 2. 建立连接
		Connection con = DriverManager.getConnection(url, "root", "123456");
		String sql = "insert into user(username,password) values( ? ,?) ";
		// 3. 创建Statement
		//Statement stm = con.createStatement();
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, username);
		stm.setString(2, password);
		// 4. 执行SQL语句
		int count = stm.executeUpdate();
		// 5. 处理结果
		System.out.println("count = " + count);
		// 6. 释放资源
		stm.close();
		con.close();

		response.sendRedirect("login_bootstrap.jsp");
	} else {
		// 获取失败，返回当前页
		response.sendRedirect("register_bootstrap.jsp");
	}
%>