<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>

<title>插入学生信息</title>
</head>
<body>
<%@ include file='header.jsp' %>
	<div class="container" style="margin-top: 60px">
		<div class="row" style="margin-top: 100px">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">
							<span class="glyphicon glyphicon-console"></span> Register
						</h3>
					</div>
					<div class="panel-body">
						
						<form id="login" action="doinsert_stu.jsp" method="post">
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control"	name="id" placeholder="学生学号">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control"	name="name" placeholder="学生姓名">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<label class="radio-inline">
									  <input type="radio" name="sex" value="男"> 男
									</label>
									<label class="radio-inline">
									  <input type="radio" name="sex" value="女">女
									</label>
									
								</div>
							</div>
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control"	name="age" placeholder="年龄">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control"	name="weight" placeholder="体重">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group col-md-8 col-md-offset-2">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-user"></span>
									</div>
									<input type="text" class="form-control"	name="height" placeholder="身高">
								</div>
							</div>
							
								<div class="form-group">
									<div class="col-md-6 col-md-offset-3">
										<button type="submit" class="btn btn-primary btn-block">提交</button>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>