<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
<title>Insert title here</title>

</head>
<body>

	<%-- <%@ include file='header.jsp'%> --%>
	<div class="container" style="margin-top: 60px">
		<div class="row">
			<div class="col-md-3">
				<ul>
					<li><a href="show" target="myframe">显示所有学生</a></li>
					<li><a href="addStudent" target="myframe">添加学生</a></li>
					<li><a href="calculator.jsp" target="myframe">计算器</a></li>
					<li><a href="316-3.jsp" target="myframe">316-3</a></li>
				</ul>
			</div>
			<div class="col-md-9">
				<iframe name="myframe" id="myframe" src="316-4.jsp" width="100%"
					height="100%" scrolling="no"></iframe>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function setIframeHeight(iframe) {
			if (iframe) {
				var iframeWin = iframe.contentWindow
						|| iframe.contentDocument.parentWindow;
				if (iframeWin.document.body) {
					iframe.height = iframeWin.document.documentElement.scrollHeight
							|| iframeWin.document.body.scrollHeight;
				}
			}
		};

		window.onload = function() {
			setIframeHeight(document.getElementById('myframe'));
		};
	</script>
</body>
</html>