<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>我是周老师，是《Java Web高级应用与开发》的任课老师</h2>
	<h3>
	<% String username = (String)application.getAttribute("username"); %>
	
	<% 
		Cookie[] cookies = request.getCookies();
		if(cookies !=null && cookies.length>0){
			for(Cookie c : cookies){
				out.print(c.getName() + " = " + c.getValue());
			}
		}
	%>已经登录</h3>
</body>
</html>